package com.intro;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.intro.ForEach.Folks.friends;

public class ForEach {
    public static void main(String[] args) {
        iterateJava7();
        iterateJava8();
    }

    public static class Folks {

        public static
        final List<String> friends =
                Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");
        public static

        final List<String> editors =
                Arrays.asList("Brian", "Jackie", "John", "Mike");

        public static

        final List<String> comrades =
                Arrays.asList("Kate", "Ken", "Nick", "Paula", "Zach");

    }

    private static void iterateJava7() {
        for (int i = 0; i < friends.size(); i++) {
            System.out.println(friends.get(i));
        }

        for (String name : friends) {
            System.out.println(name);
        }
        Iterator<String> it = friends.iterator();
        while (it.hasNext()){
            String name = it.next();
            System.out.println(name);
        }

    }


    private static void iterateJava8() {


        System.out.println("//" + "START:INTERNAL_FOR_EACH_OUTPUT");

        friends.forEach(new Consumer<String>() {
            public void accept(final String name) {
                System.out.println(name);
            }
        });

        System.out.println("//" + "END:INTERNAL_FOR_EACH_OUTPUT");

        /* ============ pay attention to following 3 syntax ============ */

        System.out.println("//" + "START:INTERNAL_OUTPUT");
        friends.forEach((final String name) -> System.out.println(name));
        System.out.println("//" + "END:INTERNAL_OUTPUT");

        friends.forEach((name) -> System.out.println(name));

        friends.forEach(name -> System.out.println(name));

        friends.forEach(System.out::println); // <--------------- most elegant !!!

    }
}
