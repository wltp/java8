package com.intro;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class MethodSynthesis {
    public static void main(String[] args) {

        List<User> list = Arrays.asList(new User(true));

        //use static method
        boolean isReal = list.stream().anyMatch(u -> User.isRealUserStatic(u));
        list.stream().anyMatch(User::isRealUserStatic);

        System.out.println(isReal);

        //use instance method
        boolean isUserReal = list.stream().anyMatch(User::isReal);

        System.out.println(isUserReal);


        //Refer to the constructor
        Function<Boolean, User> f = real -> new User(real);
        f.apply(true);

        f = User::new;//same as above
        f.apply(true);
    }

    public static class User {

        boolean real = false;

        public boolean isReal(){
            return real;
        }

        public static boolean isRealUserStatic(User u) {
            return u.isReal();
        }

//        public User(){
//
//        }

        public User(boolean real){
            this.real = real;
        }

    }
}
