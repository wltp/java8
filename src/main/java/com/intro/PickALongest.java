package com.intro;

import java.util.Optional;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;

import static com.intro.ForEach.Folks.friends;

public class PickALongest {
    public static void main(final String[] args) {
        {
            System.out.println("//" + "START:SUM_OUTPUT");

            //implements int applyAsInt(T value)
            ToIntFunction<String> stringToIntFunction = name -> name.length();

            IntStream streamOfIntegers = friends.stream().mapToInt(stringToIntFunction);
            System.out.println("Total number of characters in all names: " + streamOfIntegers.sum());
            System.out.println("//" + "END:SUM_OUTPUT");
        }

        System.out.println("//" + "START:REDUCE_OUTPUT");
        final Optional<String> aLongName =
                friends.stream().reduce((name1, name2) -> name1.length() >= name2.length() ? name1 : name2);

        aLongName.ifPresent(name -> System.out.println(String.format("A longest name: %s", name)));

        System.out.println("//" + "END:REDUCE_OUTPUT");

        final String steveOrLonger =
                friends.stream().reduce("Steve", (name1, name2) -> name1.length() >= name2.length() ? name1 : name2);

        System.out.println(steveOrLonger);
    }
}
