package com.intro;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static com.intro.IntroSnippets.Prices.prices;

public class IntroSnippets {

    public static void main(String[] args) {
        findPricesJava7();
        findPricesJava8();
    }

    public static class Prices {
        public static
        final List<BigDecimal> prices = Arrays.asList(
                new BigDecimal("10"), new BigDecimal("30"), new BigDecimal("17"),
                new BigDecimal("20"), new BigDecimal("15"), new BigDecimal("18"),
                new BigDecimal("45"), new BigDecimal("12"));
    }

    /**
     * Find prices greater than $20, discounted by 10%
     */
    public static void findPricesJava7() {
        //declare a mutable variable (source of bugs)
        BigDecimal totalOfDiscountedPrices = BigDecimal.ZERO;

        //for loop, procedural
        for (BigDecimal price : prices) {
            //if statement (source of bugs)
            //calculation looks unreadable
            if (price.compareTo(BigDecimal.valueOf(20)) > 0)
                totalOfDiscountedPrices =
                        totalOfDiscountedPrices.add(price.multiply(BigDecimal.valueOf(0.9)));
        }
        System.out.println("Total of discounted prices: " + totalOfDiscountedPrices);

    }

    /**
     * Java 8 style
     * usage of filter and map/reduce
     * more explanation in later samples
     *
     * stream prices
     * filter those which are more than 20
     * discount them to 90%
     * add all of them, with starting price of zero
     */

    public  static void findPricesJava8(){
        final BigDecimal totalOfDiscountedPrices =
                prices.stream()
                        .filter(price -> price.compareTo(BigDecimal.valueOf(20)) > 0)
                        .map(price -> price.multiply(BigDecimal.valueOf(0.9)))
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Total of discounted prices: " + totalOfDiscountedPrices);
    }
}
